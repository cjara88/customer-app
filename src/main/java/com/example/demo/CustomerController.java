package com.example.demo;

import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin
@RestController
@RequestMapping("/customers")
public class CustomerController {

	@Autowired
	private CustomerRepository repository;

	@GetMapping
	Collection<Customer> get() {
		return repository.findAll();
	}
	
	@GetMapping("/page")
	Page<Customer> get(@RequestParam int page, @RequestParam int pageSize) {
		return repository.findAll(PageRequest.of(page, pageSize));
	}

	@GetMapping("{id}")
	Customer finById(@PathVariable Integer id) {
		return repository.findById(id).orElse(null);
	}

	@PostMapping()
	Customer save(@RequestBody Customer newCustomer) {
		return repository.save(newCustomer);
	}

	@PutMapping
	Customer update(@RequestBody Customer customer) {
		return repository.save(customer);
	}

	@DeleteMapping("/{id}")
	void delete(@PathVariable Integer id) {
		repository.deleteById(id);
	}

}

