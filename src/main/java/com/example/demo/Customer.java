package com.example.demo;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;

@Entity
public class Customer implements Serializable {
	
	private static final long serialVersionUID = -5058253970849688769L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "first_name", nullable = false)
	@NotNull(message = "First name is required.")
	private String firstName;

	@Column(name = "last_name", nullable = false)
	@NotNull(message = "Last name is required.")
	private String lastName;
	
	@Column(name = "phone_number", nullable = false)
	@NotNull(message = "Phone name is required.")
	private String phoneNumber;
	
	@Column(name = "email", nullable = false)
	@NotNull(message = "Email is required.")
	@Email(message = "Email no válido.")
	private String email;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public String getPhoneNumber() {
		return phoneNumber;
	}

	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}
	
}
