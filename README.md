Customer App
----------------------------------------------

Aplicación CRUD para pruebas del workshop de Frontend, utiliza una base de datos en memoria.


#### Requisitos

* OpenJDK 1.8+
* Maven 3.3+-

#### Deploy

* Paso 1
```bash
mvn clean package -DskipTests
```
* Paso 2
```bash
mvn spring-boot:run
```
o también se puede utilizar
```bash
java -jar target/customer-app-0.0.1-SNAPSHOT.jar
```

